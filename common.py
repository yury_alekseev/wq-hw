def ticker2stock_id(ticker):
    # Remove leading SE
    ticker = ticker[2:]
    ticker_split = ticker.split('_')
    se_id = ticker_split[0]
    ticker_value = ticker_split[1]
    return int(se_id) * 100000 + int(ticker_value)


def stock_id2ticker(hash_value):
    ticker_value = str(hash_value % 100000)
    se_id = str(hash_value / 100000)
    return 'SE' + se_id + '_' + ticker_value
