#!/usr/bin/env python
# -*- coding: utf-8 -*-

import common
import sqlite3
import argparse
import datetime


class SeData:
    def __init__(self, db):
        self.data = {}

        conn = sqlite3.connect(db)

        c = conn.cursor()

        c.execute(' select SE.name, P.stock_id, P.date, P.price \
                    from Price as P, Stock as S, "Stock Exchange" as SE \
                    where P.stock_id = S.id and S.se_id = SE.id')

        for row in c:
            se = row[0]
            stock_id = row[1]
            date = row[2]
            price = row[3]

            if se not in self.data:
                self.data[se] = {}

            if date not in self.data[se]:
                self.data[se][date] = {}

            self.data[se][date][stock_id] = price

        conn.close()

    def do_item34(self):
        se_data = {}
        for se in self.data:
            se_data[se] = {}
            for date in self.data[se]:
                for stock_id in self.data[se][date]:
                    if stock_id not in se_data[se]:
                        se_data[se][stock_id] = [date, date]

                    if date < se_data[se][stock_id][0]:
                        se_data[se][stock_id][0] = date
                    if date > se_data[se][stock_id][1]:
                        se_data[se][stock_id][1] = date

        returns = {}
        for se in se_data:
            if se not in returns:
                returns[se] = {}

            for stock_id in se_data[se]:
                if stock_id not in returns[se]:
                    start_date = se_data[se][stock_id][0]
                    end_date = se_data[se][stock_id][1]

                    start_value = self.data[se][start_date][stock_id]
                    end_value = self.data[se][end_date][stock_id]
                    returns[se][stock_id] = (end_value - start_value) / start_value

        sorted_returns = {}
        for se in returns:
            sorted_returns[se] = sorted(returns[se], key=returns[se].__getitem__)

        print 'ITEM34'
        for se in sorted_returns:
            print se, 'top5: ', [common.stock_id2ticker(v) for v in sorted_returns[se][-6:-1]], ' bottom 5: ', [common.stock_id2ticker(v) for v in sorted_returns[se][:5]]

    def do_item34_slow(self):
        c = self.conn.cursor()

        se_data = {}

        for se in self.data:
            se_id = se[2:]

            print 'ITEM34'

            c.execute('select stock_id, return from (select distinct Stock.se_id as se_id, start_stock_id as stock_id, (end_price - start_price)/start_price as return from (select stock_id as start_stock_id, price as start_price from Price where Price.date in (select min(Price.date) from Price where Price.stock_id = stock_id)), (select stock_id as end_stock_id, price as end_price from Price where Price.date in (select max(Price.date) from Price where Price.stock_id = stock_id)), Stock where start_stock_id = end_stock_id and start_stock_id = Stock.id and end_stock_id = Stock.id) where se_id=:id order by return desc limit 5', {'id': se_id})

            for row in c:
                print '{:>4} {:>10} {:.3}'.format(se, common.stock_id2ticker(row[0]), row[1])

            c.execute('select stock_id, return from (select distinct Stock.se_id as se_id, start_stock_id as stock_id, (end_price - start_price)/start_price as return from (select stock_id as start_stock_id, price as start_price from Price where Price.date in (select min(Price.date) from Price where Price.stock_id = stock_id)), (select stock_id as end_stock_id, price as end_price from Price where Price.date in (select max(Price.date) from Price where Price.stock_id = stock_id)), Stock where start_stock_id = end_stock_id and start_stock_id = Stock.id and end_stock_id = Stock.id) where se_id=:id order by return limit 5', {'id': se_id})

            for row in c:
                print '{:>4} {:>10} {:.3}'.format(se, common.stock_id2ticker(row[0]), row[1])

    def do_item5(self):
        res = {}

        for se in self.data:
            res[se] = []
            se_stock_ids = {}
            for date in self.data[se]:
                for stock_id in self.data[se][date]:
                    se_stock_ids[stock_id] = 1
            for date in self.data[se]:
                if len(self.data[se][date]) != len(se_stock_ids):
                    res[se].append(datetime.datetime.fromtimestamp(date).strftime('%Y%m%d'))

        print 'ITEM5'
        for se in res:
            print se + ':', ' '.join(res[se])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--db', default='se.db')
    args = parser.parse_args()

    se_data = SeData(args.db)

    se_data.do_item34()

    se_data.do_item5()


if __name__ == '__main__':
    main()
