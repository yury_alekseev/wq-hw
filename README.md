wq-hw
===========

Home work for Worldquant

Usage
=====

````bash
user@host:~/wq-hw/parse_se_data.py --input-directory ./data/  --output-directory ./out/ --output-db se.db
user@host:~/wq-hw/query_db.py --db se.db

```

