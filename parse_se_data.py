#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import common
import sqlite3
import argparse


class SeData:
    def __init__(self, in_dir):
        self.data = {}

        for root, subdirs, files in os.walk(in_dir):
            for name in files:
                file_name = os.path.join(root, name)
                self.read_file(file_name)

    def make_dir(self, dir_name):
        try:
            os.makedirs(dir_name)
        except OSError:
            if not os.path.isdir(dir_name):
                raise

    def remove_file(self, file_name):
        try:
            os.remove(file_name)
        except OSError:
            if os.path.exists(file_name):
                raise

    def fmt2sep(self, fmt):
        if fmt == 'bar':
            return '|'
        if fmt == 'csv':
            return ','
        if fmt == 'space':
            return ' '
        raise Exception('Unknown format: ' + fmt)

    def read_all_file(self, file_name):
        name_split = file_name.split('_')
        fmt = name_split[-2]
        se = name_split[-3]

        sep = self.fmt2sep(fmt)

        if se not in self.data:
            self.data[se] = {}

        with open(file_name) as f:
            for line in f:
                line_split = line.split(sep)

                date = line_split[0]

                if date not in self.data[se]:
                    self.data[se][date] = {}

                for (ticker, value) in zip(line_split[1::2], line_split[2::2]):
                    self.data[se][date][ticker] = value

    def read_daily_file(self, file_name):
        file_name_split = file_name.split('_')
        date = file_name_split[-1].split('.')[0]
        fmt = file_name_split[-2]
        se = file_name_split[-3]

        sep = self.fmt2sep(fmt)

        if se not in self.data:
            self.data[se] = {}

        if date not in self.data[se]:
            self.data[se][date] = {}

        with open(file_name) as f:
            # Skip the date line
            next(f, None)

            for line in f:
                line = line.rstrip()
                line = line.rstrip(sep)

                line_split = line.split(sep)

                ticker = line_split[0].strip()
                value = line_split[1].strip()
                self.data[se][date][ticker] = value

    def read_file(self, file_name):
        date = file_name.split('_')[-1].split('.')[0]
        if date == 'all':
            self.read_all_file(file_name)
        else:
            self.read_daily_file(file_name)

    def save_to_dir(self, out_dir):
        self.make_dir(out_dir)

        for se in self.data:
            se_dir = os.path.join(out_dir, se)
            self.make_dir(se_dir)

            all_file = os.path.join(se_dir, 'csv_all')
            self.remove_file(all_file)

            for date in self.data[se]:
                out_file = os.path.join(se_dir, 'csv_' + date)

                with open(out_file, 'w') as f:
                    for ticker in sorted(self.data[se][date], key=lambda x: int(x.split('_')[1])):
                        print >>f, ticker + ',' + self.data[se][date][ticker]

                with open(all_file, 'a') as f:
                    f.write(date)
                    for ticker in sorted(self.data[se][date], key=lambda x: int(x.split('_')[1])):
                        f.write(',' + ticker + ',' + self.data[se][date][ticker])
                    f.write('\n')

    def save_to_sql(self, db_name):
        conn = sqlite3.connect(db_name)

        c = conn.cursor()

        c.execute('DROP TABLE IF EXISTS "Stock Exchange"')
        c.execute('''CREATE TABLE "Stock Exchange" (
            id INT PRIMARY KEY,
            name TEXT);''')

        c.execute('DROP TABLE IF EXISTS "Stock"')
        c.execute('''CREATE TABLE "Stock" (
            id INT PRIMARY KEY,
            ticker TEXT,
            se_id INT);''')

        c.execute('DROP TABLE IF EXISTS "Price"')
        c.execute('''CREATE TABLE "Price" (
            stock_id INT,
            date INT,
            price REAL);''')

        for se in self.data:
            se_id = se[2:]
            c.execute('INSERT INTO "Stock Exchange" (id, name) \
                VALUES (?, ?)', (se_id, se))

            for date in self.data[se]:
                ts = int(time.mktime(time.strptime(date, "%Y%m%d")))

                for ticker in self.data[se][date]:
                    stock_id = common.ticker2stock_id(ticker)

                    try:
                        c.execute('INSERT INTO "Stock" (id, ticker, se_id) \
                            VALUES (?, ?, ?)', (stock_id, ticker, se_id))
                    except sqlite3.IntegrityError:
                        pass

                    c.execute('INSERT INTO "Price" (stock_id, date, price) \
                        VALUES (?, ?, ?)', (stock_id, ts, float(self.data[se][date][ticker])))

        conn.commit()
        conn.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-directory', default='data')
    parser.add_argument('--output-directory', default='out')
    parser.add_argument('--output-db', default='se.db')

    args = parser.parse_args()

    se_data = SeData(args.input_directory)

    se_data.save_to_dir(args.output_directory)

    se_data.save_to_sql(args.output_db)

if __name__ == '__main__':
    main()
